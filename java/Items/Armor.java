package Items;

/*
 * Classe Armor
 *  Representa uma armadura no jogo
*/
public class Armor extends Item
{
    protected int defensepts;

    protected double weight;

    /*
     * Nome: Armor (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (String) nome da armadura, (double) preço, (int) pontos de defesa, (double) peso
    */
    public Armor(String name, double price, int defensepts, double weight)
    {
        super(name, price);

        // Se os pontos de defesa estiverem dentro do intervalo permitido
        if(defensepts >= 1 && defensepts <= 20) 
        {
            this.defensepts = defensepts;
        }
        else
        {
            this.defensepts = 1; // Atribui valor padrao
            System.err.println("Erro em Armor.Armor(String, double, int, int): defensepts deve estar entre 1 e 20.");
        }

        if(weight >= 1 && weight <= 20)
        {
            this.weight = weight;
        }
        else
        {
            this.weight = 1; // Atribui valor padrao
            System.err.println("Erro em Armor.Amor(String, double, int, int) : weight deve estar entre 1 e 20.");
        }
    }

    /*
     * Nome: Armor (Construtor de cópia)
     * Descricao: Construtor que copia os atributos de um objeto Armor
     * Entrada: (Armor) Referencia para um objeto Armor
    */
    public Armor(Armor armor)
    {
        super(armor);

        defensepts = armor.defensepts;
        weight = armor.weight;
    }

    /*
     * Nome: getDefensePts
     * Descricao: retorna os pontos de defesa da armadura
     * Saida: (int) pontos de defesa
    */
    public int getDefensePts()
    {
        return defensepts;
    }
    
    /*
     * Nome: getAttackPts
     * Descricao: retorna os pontos de ataque da armadura, nao usado nessa classe
     * Saida: (int) 0
    */
    public int getAttackPts()
    {
        return 0;
    }

    /*
     * Nome: getWeight
     * Descricao: retorna o peso da armadura
     * Saida: (double) peso
    */
    public double getWeight()
    {
        return weight;
    }
}
