package Items;

/*
 * Classe ManaPotion
 *  Representa uma poção de magia no jogo
*/
public class ManaPotion extends Potion
{
    /*
     * Nome: ManaPotion (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (String) nome da pocao, (double) preço, (int) poder de restauracao de mana
    */
    public ManaPotion(String name, double price, int restorepts)
    {
        super(name, price, restorepts);
    }

    /*
     * Nome: use
     * Descricao: usa a pocao
     * Entrada: (Character) personagem usando a pocao
     * Saida: (void)
    */
    public void use(Characters.Character character)
    {
        character.addMP(getDefensePts());

        character.removeItem(getName());
    }
}
