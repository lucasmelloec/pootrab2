package Items;

/*
 * Classe HealthPotion
 *  Representa uma poção de vida no jogo
*/
public class HealthPotion extends Potion
{
    /*
     * Nome: HealthPotion (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (String) nome da pocao, (double) preço, (int) poder de restauracao de vida
    */
    public HealthPotion(String name, double price, int restorepts)
    {
        super(name, price, restorepts);
    }

    /*
     * Nome: use
     * Descricao: usa a pocao
     * Entrada: (Character) personagem usando a pocao
     * Saida: (void)
    */
    public void use(Characters.Character character)
    {
        character.addHP(getDefensePts());

        character.removeItem(getName());
    }
}
