package Items;

/*
 * Classe Potion
 *  Representa uma poção no jogo
*/
public abstract class Potion extends Item
{
    private int restorepts;

    /*
     * Nome: Potion (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (String) nome da pocao, (double) preço, (int) poder de restauracao
    */
    public Potion(String name, double price, int restorepts)
    {
        super(name, price);

        this.restorepts = restorepts;
    }

    /*
     * Nome: Potion (Construtor de cópia)
     * Descricao: Construtor que copia os atributos de um objeto Potion
     * Entrada: (Potion) Referencia para um objeto Potion
    */
    public Potion(Potion potion)
    {
        super(potion);

        restorepts = potion.restorepts;
    }

    /*
     * Nome: getDefensePts
     * Descricao: retorna os pontos de restauracao da pocao
     * Saida: (int) Pontos de restauracao
    */
    public int getDefensePts()
    {
        return restorepts;
    }

    /*
     * Nome: getAttackPts
     * Descricao: retorna os pontos de ataque do item, nao usado nessa classe
     * Saida: (int) 0
    */
    public int getAttackPts()
    {
        return 0;
    }

    /*
     * Nome: use
     * Descricao: método puramente virtual, usa a pocao
     * Entrada: (Character) personagem usando a pocao
     * Saida: (void)
    */
    public abstract void use(Characters.Character character);
}
