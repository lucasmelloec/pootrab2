package Items;

/*
 * Classe Item
 *  Representa um item no jogo
*/
public abstract class Item
{
    private String name; // Nome do item
    private double price; // Preço do item

    /*
     * Nome: Item (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (String) nome do item, (double) preço
    */
    public Item(String name, double price)
    {
        this.name = name;
        this.price = price;
    }

    /*
     * Nome: Item (Construtor de copia)
     * Descricao: Construtor que copia os atributos de um objeto Item
     * Entrada: (Item) referencia ao objeto Item
    */
    public Item(Item item)
    {
        name = item.name;
        price = item.price;
    }

    /*
     * Nome: getName
     * Descricao: Retorna o nome do item
     * Saida: (String) nome
    */
    public String getName()
    {
        return name;
    }

    /*
     * Nome: getPrice
     * Descricao: Retorna o preço do item
     * Saida: (double) preço
    */
    public double getPrice()
    {
        return price;
    }

    /*
     * Nome: use
     * Descricao: metodo virtual, utiliza um item
     * Entrada: (Character) personagem usando a pocao
     * Saida: (void)
    */
    public void use(Characters.Character character)
    {
    }

    /*
     * Nome: getDefensePts
     * Descricao: metodo puramente virtual, retorna os pontos de defesa do item
     * Saida: (int) Pontos de defesa
    */
    public abstract int getDefensePts();

    /*
     * Nome: getAttackPts
     * Descricao: metodo puramente virtual, retorna os pontos de ataque do item
     * Saida: (int) Pontos de ataque
    */
    public abstract int getAttackPts();
    
    /*
     * Nome: getWeight
     * Descricao: Metodo virutal, retorna o peso do item
     * Saida: (double) peso
    */
    public double getWeight()
    {
        return 0;
    }
}
