package Items;

import java.util.*;
import Utility.*;

/*
 * Classe Inventory
 *     Inventario do personagem
*/
public class Inventory
{
    private int spaces; // Espaços do inventario
    private double gold; // Ouro do personagem

    private ArrayList<Pair<Item, Boolean>> items; // Colecao de objetos Item presentes no inventario e de booleans indicando se esta equipado

    /*
     * Nome: Inventory (Construtor)
     * Descricao: Construtor que inicializa os atributos em 0 e sem items no vetor
    */
    public Inventory()
    {
        spaces = 0;
        gold = 0;

        items = new ArrayList<Pair<Item, Boolean>>();
    }

    /*
     * Nome: getTotalGold
     * Descricao: Retorna a quantidade de ouro possuida pelo personagem
     * Saida: (double) quantia de ouro
    */
    public double getTotalGold()
    {
        return gold;
    }

    /*
     * Nome: getAvailableSpace
     * Descricao: Retorna quanto espaço vazio há no inventário
     * Saida: (int) espaco disponivel
    */
    public int getAvailableSpace()
    {
        return spaces - items.size();
    }

    /*
     * Nome: spendGold
     * Descricao: Diminui a quantia de ouro do personagem
     * Entrada: (double) quantia a ser retirada
     * Saida: (void)
    */
    public void spendGold(double amount)
    {
        if(amount <= gold)
            gold -= amount;
        else
            System.err.println("Erro em Inventory.spendGold(double): o personagem nao possui ouro suficiente.");
    }

    /*
     * Nome: earnGold
     * Descricao: Aumenta a quantia de ouro do personagem
     * Entrada: (double) quantia a ser acrescida
     * Saida: (void)
    */
    public void earnGold(double amount)
    {
        gold += amount;
    }

    /*
     * Nome: setSpaces
     * Descricao: Atribui uma quantidade de espacos no inventário
     * Entrada: (int) espaço disponivel
     * Saida: (void)
    */
    public void setSpaces(int spaces)
    {
        this.spaces = spaces;
    }

    /*
     * Nome: searchItem
     * Descricao: Retorna um item do inventario
     * Entrada: (String) nome do item
     * Saida: (Item) referencia ao item com aquele nome
    */
    public Item searchItem(String itemName)
    {
        // Itera por todos os itens
        for(Pair<Item, Boolean> it : items)
            if( it.getFirst().getName().equals(itemName) == true ) // Se nome do item for o mesmo que esta sendo procurado
                return it.getFirst(); // Retorna o item

        return null;    // Retorna null caso o item nao seja encontrado
    }

    /*
     * Nome: searchItem
     * Descricao: Retorna um item do inventario
     * Entrada: (int) posicao do item
     * Saida: (Item) referencia ao item com aquela posicao no vetor
    */
    public Item searchItem(int pos)
    {
        // Se a posicao estiver abaixo do limite permitido
        if(pos < items.size())
            return items.get(pos).getFirst();

        return null; // Se a posicao do item ultrapassa o limite, retorna null
    }

    /*
     * Nome: insertItem
     * Descricao: Insere um item no inventário
     * Entrada: (Item) referencia ao item
     * Saida: (void)
    */
    public void insertItem(Item item)
    {
        // Somente insere se houver espaco disponivel no inventario
        if(items.size() < spaces)
            items.add(new Pair<Item, Boolean>(item, false));
        else
            System.err.println("Erro em Inventory.insertItem: O personagem nao possui espaco disponivel no inventario.");
    }

    /*
     * Nome: removeItem
     * Descricao: Remove um item do inventário
     * Entrada: (String) nome do item
     * Saida: (void)
    */
    public void removeItem(String itemName)
    {
        // Itera por todos os itens
        for(Pair<Item, Boolean> it : items)
        {
            if( it.getFirst().getName().equals(itemName) == true ) // Se nome do item for o mesmo que esta sendo procurado
            {
                // Tira o item do vetor
                items.remove(it); // Tira do vetor
                return; // Sai da funcao
            }
        }
    }

    /*
     * Nome: removeItem
     * Descricao: Remove um item do inventário
     * Entrada: (int) posicao do item
     * Saida: (void)
    */
    public void removeItem(int pos)
    {
        if(pos < items.size())
            items.remove(pos);
        else
            System.err.println("Erro em Inventory.removeItem: A posicao nao correponde a um item valido.");
    }

    /*
     * Nome: getNumberOfItems
     * Descricao: Retorna quantos itens existem no inventario
     * Saida: (int) quantia de itens
    */
    public int getNumberOfItems()
    {
        return items.size();
    }
    
    /*
     * Nome: equipItem
     * Descricao: Equipa um item do inventário
     * Entrada: (String) nome do item
     * Saida: (void)
    */
    public void equipItem(String itemName)
    {
        // Itera por todos os itens
        for(Pair<Item, Boolean> it : items)
        {
            if( it.getFirst().getName().equals(itemName) == true && it.getSecond() == false) // Se nome do item for o mesmo que esta sendo procurado
            {
                it.setSecond(true); // Seta para equipado

                return; // Sai da funcao
            }
        }
    }
    
    /*
     * Nome: unequipItem
     * Descricao: Desequipa um item do inventário
     * Entrada: (String) nome do item
     * Saida: (void)
    */
    public void unequipItem(String itemName)
    {
        // Itera por todos os itens
        for(Pair<Item, Boolean> it : items)
        {
            if( it.getFirst().getName().equals(itemName) == true && it.getSecond() == true) // Se nome do item for o mesmo que esta sendo procurado
            {
                it.setSecond(false); // Seta para desequipado

                return; // Sai da funcao
            }
        }
    }
    
    /*
     * Nome: isEquipped
     * Descricao: Retorna de um item esta equipado
     * Entrada: (int) posicao do item
     * Saida: (boolean) esta equipado?
    */
    public boolean isEquipped(int itemPos)
    {
        return items.get(itemPos).getSecond(); // Retorna se esta equipado ou nao
    }
}
