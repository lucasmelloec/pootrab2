package Items;

/*
 * Classe Weapon
 *  Representa uma arma no jogo
*/
public class Weapon extends Item
{
    protected int attackpts;

    protected double range;

    /*
     * Nome: Weapon (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (String) nome da arma, (double) preço, (int) pontos de ataque, (double) alcance
    */
    public Weapon(String name, double price, int attackpts, double range)
    {
        super(name, price);

        // Se os pontos de ataque estiverem dentro do intervalo permitido
        if(attackpts >= 1 && attackpts <= 9) 
        {
            this.attackpts = attackpts;
        }
        else
        {
            this.attackpts = 1; // Atribui valor padrao
            System.err.println("Erro em Weapon.Weapon(String, double, int, int): attackpts deve estar entre 1 e 9.");
        }

        this.range = range;
    }

    /*
     * Nome: Weapon (Construtor de cópia)
     * Descricao: Construtor que copia os atributos de um objeto Weapon
     * Entrada: (Weapon) Referencia para um objeto Weapon
    */
    public Weapon(Weapon weapon)
    {
        super(weapon);

        attackpts = weapon.attackpts;
        range = weapon.range;
    }

    /*
     * Nome: getDefensePts
     * Descricao: retorna os pontos de defesa, nao usado nessa classe
     * Saida: (int) 0
    */
    public int getDefensePts()
    {
        return 0;
    }

    /*
     * Nome: getRange
     * Descricao: retorna o alcance da arma
     * Saida: (double) range
    */
    public double getRange()
    {
        return range;
    }

    /*
     * Nome: getAttackPts
     * Descricao: retorna os pontos de ataque da arma
     * Saida: (int) pontos de ataque
    */
    public int getAttackPts()
    {
        return attackpts;
    }
}
