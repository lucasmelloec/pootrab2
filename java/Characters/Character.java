package Characters;

import Items.*;

public abstract class Character
{
    private String alias; // Nome do personagem

    private Inventory myitems; // Inventario do personagem

    private int HP; // Pontos de vida
    private int MP; // Pontos de magia

    private int numWeapons; // Quantas armas estao equipadas
    private int numArmors; // Quantas armaduras estao equipadas

    protected int XP; // Experiencia
    protected int strength; // Força
    protected int speed; // Velocidade
    protected int dexterity; // Destreza
    protected int constitution; // Constituição

    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos e recebe apenas alias
     * Entrada: (String) nome do personagem
    */
    public Character(String alias)
    {
        HP = 100; // Valor inicial de HP
        MP = 0; // Valor inicial de MP

        strength = speed = dexterity = constitution = 25; // Valores padroes de atributos

        XP = 1; // Valor inicial de XP

        this.alias = alias;

        myitems = new Inventory();
        myitems.setSpaces(6); // Quantidade padrao de espacos no inventario
        
        numWeapons = 0;
        numArmors = 0;
    }

    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (String) nome do personagem, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    public Character(String alias, int strength, int speed, int dexterity, int constitution, int invSpace)
    {
        HP = 100; // Valor inicial de HP
        MP = 0; // Valor inicial de MP

        this.strength = strength;
        this.speed = speed;
        this.dexterity = dexterity;
        this.constitution = constitution;

        if(strength + speed + dexterity + constitution != 100) // Se os atributos nao somarem 100
            strength = speed = dexterity = constitution = 25; // entao atribui os valores padroes

        if(strength < 1 || speed < 1 || dexterity < 1 || constitution < 1) // Se algum atributo tiver abaixo do valor minimo
            strength = speed = dexterity = constitution = 25; // entao atribui os valores padroes

        XP = 1; // Valor inicial de XP

        this.alias = alias;

        myitems = new Inventory();
        myitems.setSpaces(invSpace); // Atribui a quantidade de espacos do inventario

        numWeapons = 0;
        numArmors = 0;
    }

    /*
     * Nome: getName
     * Descricao: Retorna o alias
     * Saida: (String) nome do personagem
    */
    public String getName()
    {
        return alias;
    }

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    public int getDefensePoints() // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente
    {
        int item_def_pts = 0;

        for(int i = 0; i < myitems.getNumberOfItems(); i++)
            if(myitems.isEquipped(i) == true)
                item_def_pts += myitems.searchItem(i).getDefensePts();

        Double def_pts = ( ( constitution*0.5 + dexterity*0.3 + getSpeed()*0.2 ) + item_def_pts ) * (XP/2);

        return def_pts.intValue();
    }

    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    public int getAttackPoints() // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente
    {
        int item_att_pts = 0;

        for(int i = 0; i < myitems.getNumberOfItems(); i++)
            if(myitems.isEquipped(i) == true)
                item_att_pts += myitems.searchItem(i).getAttackPts();

        Double att_pts = ( ( strength*0.5 + dexterity*0.3 + getSpeed()*0.2 ) + item_att_pts ) * (XP/3);

        return att_pts.intValue();
    }

    /*
     * Nome: attack
     * Descricao: Metodo puramente virtual, ataca um outro personagem
     * Entrada: (Character) Referencia a outro personagem
     * Saida: (void)
    */
    public abstract void attack(Character character);

    /*
     * Nome: addXP
     * Descricao: Adiciona XP
     * Entrada: (int) xp a ser adicionada
     * Saida: (void)
    */
    public void addXP(int XP)
    {
        this.XP += XP;
        
        if(this.XP > 100)
            this.XP = 100;
        
        if(this.XP < 1)
            this.XP = 1;
    }

    /*
     * Nome: setStrength
     * Descricao: Atribui um valor de força
     * Entrada: (int) força
     * Saida: (void)
    */
    public void setStrength(int strength)
    {
        if(strength >= 1 && strength < 100)
            this.strength = strength;
    }

    /*
     * Nome: setSpeed
     * Descricao: Atribui um valor de velocidade
     * Entrada: (int) velocidade
     * Saida: (void)
    */
    public void setSpeed(int speed)
    {
        if(speed >= 1 && speed < 100)
            this.speed = speed;
    }

    /*
     * Nome: setDexterity
     * Descricao: Atribui um valor de dextreza
     * Entrada: (int) dextreza
     * Saida: (void)
    */
    public void setDexterity(int dexterity)
    {
        if(dexterity >= 1 && dexterity < 100)
            this.dexterity = dexterity;
    }

    /*
     * Nome: setConstitution
     * Descricao: Atribui um valor de constituicao
     * Entrada: (int) constituicao
     * Saida: (void)
    */
    public void setConstitution(int constitution)
    {
        if(constitution >= 1 && constitution < 100)
            this.constitution = constitution;
    }

    /*
     * Nome: addHP
     * Descricao: Adiciona um valor de HP
     * Entrada: (int) HP a ser adicionada
     * Saida: (void)
    */
    public void addHP(int HP)
    {
        this.HP += HP;

        if(this.HP > 100) // Valor maximo de HP
            this.HP = 100;

        if(this.HP <= 0)
            System.out.println(alias + " morreu.");
    }

    /*
     * Nome: addMP
     * Descricao: Adiciona um valor de MP
     * Entrada: (int) MP a ser adicionada
     * Saida: (void)
    */
    public void addMP(int MP)
    {
        this.MP += MP;

        if(this.MP > 100) // Valor maximo de MP
            this.MP = 100;

        if(this.MP < 0) // Valor minimo de MP
            this.MP = 0;
    }

    /*
     * Nome: getSpeed
     * Descricao: Retorna a velocidade modificada pela armadura
     * Saida: (int) velocidade modificada pelo peso da armadura
    */
    public int getSpeed()
    {
        double weight = 0;
        
        for(int i = 0; i < myitems.getNumberOfItems(); i++)
            if(myitems.isEquipped(i) == true)
                weight += myitems.searchItem(i).getWeight();
    
        Double modSpeed =  speed*Math.exp(- (weight * weight) );

        return modSpeed.intValue();
    }
    
    /*
     * Nome: getHP
     * Descricao: Retorna o HP do personagem
     * Saida: (int) HP
    */
    public int getHP()
    {
        return HP;
    }
    
    /*
     * Nome: insertItem
     * Descricao: Coloca um item no inventario
     * Entrada: (Item) item
     * Saida: (void)
    */
    public void insertItem(Item item)
    {
        myitems.insertItem(item);
    }

    /*
     * Nome: removeItem
     * Descricao: Tira um item no inventario
     * Entrada: (String) nome do item
     * Saida: (void)
    */
    public void removeItem(String itemName)
    {
        myitems.removeItem(itemName);
    }
    
    /*
     * Nome: equipWeapon
     * Descricao: Equipa uma arma do inventario
     * Entrada: (String) nome da arma
     * Saida: (void)
    */
    public void equipWeapon(String weaponName)
    {
        if(numWeapons >= 2)
        {
            System.err.println("Inventory.equipWeapon(String): Ja existem duas armas equipadas.");
        }
        else
        {
            myitems.equipItem(weaponName);
            numWeapons++;
        }
    }
    
    /*
     * Nome: equipArmor
     * Descricao: Equipa uma armadura do inventario
     * Entrada: (String) nome da armadura
     * Saida: (void)
    */
    public void equipArmor(String armorName)
    {
        if(numArmors >= 1)
        {
            System.err.println("Inventory.equipArmor(String): Ja existe uma armadura equipada.");
        }
        else
        {
            myitems.equipItem(armorName);
            numArmors++;
        }
    }
    
    /*
     * Nome: unequipWeapon
     * Descricao: Desequipa uma arma
     * Entrada: (String) nome da arma
     * Saida: (void)
    */
    public void unequipWeapon(String weaponName)
    {
        if(numWeapons <= 0)
        {
            System.err.println("Inventory.unequipWeapon(String): Nao existem armas equipadas.");
        }
        else
        {
            myitems.unequipItem(weaponName);
            numWeapons--;
        }
    }
    
    /*
     * Nome: unequipArmor
     * Descricao: Desequipa uma armadura
     * Entrada: (String) nome da armadura
     * Saida: (void)
    */
    public void unequipArmor(String armorName)
    {
        if(numArmors <= 0)
        {
            System.err.println("Inventory.unequipArmor(String): Nao existe armadura equipada.");
        }
        else
        {
            myitems.unequipItem(armorName);
            numArmors--;
        }
    }
}
