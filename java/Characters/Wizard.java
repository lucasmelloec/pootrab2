package Characters;

/*
 * Classe Wizard
 *     Tipo de personagem (mago)
*/
public class Wizard extends Character
{
    protected int wisdom; // sabedoria

    /*
     * Nome: Wizard (Construtor)
     * Descricao: Construtor que inicia os atributos
     * Entrada: (String) nome do personagem, (int) sabedoria
    */
    public Wizard(String alias, int wisdom)
    {
        super(alias); // Chama o construtor de Character

        this.wisdom = wisdom;
    }

    /*
     * Nome: Wizard (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (String) nome do personagem, (int) sabedoria, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    public Wizard(String alias, int wisdom, int strength, int speed, int dexterity, int constitution, int invSpace)
    {
        super(alias, strength, speed, dexterity, constitution, invSpace); // Chama o construtos de Character

        this.wisdom = wisdom;
    }
    
    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    public int getAttackPoints() // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente
    {
        return super.getAttackPoints();
    }

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    public int getDefensePoints() // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente
    {
        Double def_pts = super.getDefensePoints() + ( (double)wisdom/2 );
        return def_pts.intValue();
    }

    /*
     * Nome: attack
     * Descricao: ataca um outro personagem
     * Entrada: (Character) Referencia a outro personagem
     * Saida: (void)
    */
    public void attack(Character character)
    {
        double miss_chance = 0.1 / XP; // Chance de miss

        int damage = 0;

        if( Math.random() <= miss_chance) // Se o personagem errou
        {
            System.out.println("MISS de " + super.getName());
        }
        else // Se acertou o golpe
        {
            int rnd = (int)(Math.random() * 11) - 5; // Gera um int entre -5 e 5
            
            damage = (getAttackPoints() - character.getDefensePoints()) + rnd; // Calcula o dano
            
            // Se o dano for menor ou igual a zero, utiliza dano 1
            if(damage <= 0)
                damage = 1;

            double critical_chance = 0.02*(XP/2); // Chance de ataque critico

            // Se o personagem conseguiu um ataque critico
            if( Math.random() <= critical_chance )
            {
                damage *= 2;
                System.out.println("CRITICO");
            }

            character.addHP(-damage);
        }

        System.out.println(super.getName() + " causou " + damage + " de dano em " + character.getName());
        System.out.println("    " + character.getName() + " ficou com " + character.getHP() + " de HP.");
    }

    /*
     * Nome: addWisdom
     * Descricao: Adiciona sabedoria
     * Entrada: (int) sabedoria a ser adicionada(ou removido se for negativo)
     * Saida: (void)
    */
    public void addWisdom(int wisdom)
    {
        this.wisdom += wisdom;

        if(wisdom < 0) // O valor minimo de power é 0
            wisdom = 0;
    }
}
