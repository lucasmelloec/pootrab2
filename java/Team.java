import java.util.*;
import Characters.*;

public class Team
{
    private String name;
    
    private Color color;
    
    private int win; // Quantidade de vitorias
    private int lose; // Quatidade de derrotas
    private int draw; // Quantidade de empates

    private ArrayList<Characters.Character> characters; // Colecao de objetos Character

    /*
     * Nome: Team (Construtor)
     * Descricao: Construtor que inicia os atributos
     * Entrada: (String) nome do personagem, (Color) cor do time
    */
    public Team(String name, Color color)
    {
        this.name = name;
        this.color = color;

        win = 0;
        lose = 0;
        draw = 0;

        characters = new ArrayList<Characters.Character>();
    }

    /*
     * Nome: getName
     * Descricao: Retorna o nome
     * Saida: (String) nome do personagem
    */
    public String getName()
    {
        return name;
    }
    
    /*
     * Nome: getResults
     * Descricao: Retorna os resultados do time
     * Saida: (String) resultados do time
    */
    public String getResults()
    {
        return "Vitorias: " + win + ".  Derrotas: " + lose + ".  Empates: " + draw + ".";
    }

    /*
     * Nome: toString
     * Descricao: Retorna o nome e a cor do time
     * Saida: (String) Nome e cor do time
    */
    public String toString()
    {
        return "Nome do Time: " + name + ".  Cor do Time: " + color + ".";
    }

    /*
     * Nome: resolveBattle
     * Descricao: Resolve uma batalha com um outro time, verificando os pontos e adicionando uma vitoria, derrota ou empate
     * Entrada: (Team) Time com o qual a batalha deve ser resolvida
     * Saida: (int) -1 se o time perdeu, 0 se empatou e 1 se ganhou
    */
    public int resolveBattle(Team teamB)
    {
        double pointsA = getPoints();
        double pointsB = teamB.getPoints();

        // Time A ganhou
        if(pointsA > pointsB)
        {
            win++;
            return 1;
        }
        // Time A perdeu
        else if(pointsA < pointsB)
        {
            lose++;
            return -1;
        }

        // Caso de empate
        draw++;
        return 0;
    }

    /*
     * Nome: addChar
     * Descricao: Adiciona um personagem ao time
     * Entrada: (Character) Personagem que sera adicionado
     * Saida: (void)
    */
    public void addChar(Characters.Character character)
    {
        characters.add(character);
    }

    /*
     * Nome: removeChar
     * Descricao: Remove um personagem do time
     * Entrada: (int) Posicao do personagem no vetor
     * Saida: (void)
    */
    public void removeChar(int pos)
    {
        // Se a posicao estiver abaixo do limite permitido
        if(pos < characters.size())
            characters.remove(pos);
    }

    /*
     * Nome: removeChar
     * Descricao: Remove um personagem do time
     * Entrada: (Character) Personagem que sera removido
     * Saida: (void)
    */
    public void removeChar(Characters.Character character)
    {
        characters.remove(character);
    }

    /*
     * Nome: searchChar
     * Descricao: Retorna um personagem do time
     * Entrada: (String) Nome do personagem
     * Saida: (Character) Personagem com o nome procurado
    */
    public Characters.Character searchChar(String characterName)
    {
        // Itera por todos os personagens
        for(Characters.Character it : characters)
            if( it.getName().equals(characterName) == true) // Se nome do personagem for o mesmo que esta sendo procurado
                return it; // Retorna o personagem

        return null; // Retorna null caso o personagem nao seja encontrado
    }

    /*
     * Nome: searchChar
     * Descricao: Retorna um personagem do time
     * Entrada: (int) Posicao do personagem
     * Saida: (Character) Personagem na posicao procurada
    */
    public Characters.Character searchChar(int characterpos)
    {
        return characters.get(characterpos);
    }

    /*
     * Nome: getPoints
     * Descricao: Pontuacao do time, dada pela media do HP dos personagens 
     * Saida: (double) Pontuacao
    */
    public double getPoints()
    {
        double points = 0;

        // Itera por todos os personagens
        for(Characters.Character it : characters)
            points += it.getHP();

        points /= (double)characters.size();

        return points;
    }
}
