package Utility;

public class Pair <First_Type, Second_Type>
{
    private First_Type first_element;
    private Second_Type second_element;

    public Pair(First_Type first, Second_Type second)
    {
        first_element = first;
        second_element = second;
    }

    public void setFirst(First_Type first)
    {
        first_element = first;
    }

    public void setSecond(Second_Type second)
    {
        second_element = second;
    }

    public First_Type getFirst()
    {
        return first_element;
    }

    public Second_Type getSecond()
    {
        return second_element;
    }
}
