#ifndef _KNIGHT_HPP_
#define _KNIGHT_HPP_

#include <string>

#include "Character.hpp"

/*
 * Classe Knight
 *     Tipo de personagem (cavaleiro)
*/
class Knight : public Character
{
    protected:
    int power; // Poder de resistencia

    public:
    /*
     * Nome: Knight (Construtor)
     * Descricao: Construtor que inicia os atributos
     * Entrada: (std::string) nome do personagem, (int) poder de resistencia
    */
    Knight(std::string alias, int power);

    /*
     * Nome: Knight (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (std::string) nome do personagem, (int) poder de resistencia, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    Knight(std::string alias, int power, int strength, int speed, int dexterity, int constitution, int invSpace);

    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    int getAttackPoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    int getDefensePoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: attack
     * Descricao: ataca um outro personagem
     * Entrada: (Character*) Referencia a outro personagem
     * Saida: (void)
    */
    void attack(Character* character);

    /*
     * Nome: addPower
     * Descricao: Adiciona poder de resistencia
     * Entrada: (int) power a ser adicionada(ou removido se for negativo)
     * Saida: (void)
    */
    void addPower(int power);
};

#endif
