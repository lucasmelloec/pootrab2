#ifndef _TEAM_HPP_
#define _TEAM_HPP_

#include <string>
#include <vector>

#include "Character.hpp"
#include "Color.hpp"

class Team
{
    private:
    std::string name;
    
    enum Color color;
    
    int win; // Quantidade de vitorias
    int lose; // Quatidade de derrotas
    int draw; // Quantidade de empates

    std::vector<Character*> characters; // Colecao de objetos Character

    public:
    /*
     * Nome: Team (Construtor)
     * Descricao: Construtor que inicia os atributos
     * Entrada: (std::string) nome do personagem, (enum Color) cor do time
    */
    Team(std::string name, enum Color color);

    /*
     * Nome: ~Team (Destrutor)
     * Descricao: Destrutor que elimina todos os personagens
    */
    ~Team();

    /*
     * Nome: getName
     * Descricao: Retorna o nome
     * Saida: (std::string) nome do personagem
    */
    std::string getName();
    
    /*
     * Nome: getResults
     * Descricao: Retorna os resultados do time
     * Saida: (std::string) resultados do time
    */
    std::string getResults();

    /*
     * Nome: toString
     * Descricao: Retorna o nome e a cor do time
     * Saida: (std::string) Nome e cor do time
    */
    std::string toString();

    /*
     * Nome: resolveBattle
     * Descricao: Resolve uma batalha com um outro time, verificando os pontos e adicionando uma vitoria, derrota ou empate
     * Entrada: (Team&) Time com o qual a batalha deve ser resolvida
     * Saida: (int) TODO
    */
    int resolveBattle(Team & teamB);

    /*
     * Nome: addChar
     * Descricao: Adiciona um personagem ao time
     * Entrada: (Character*) Personagem que sera adicionado
     * Saida: (void)
    */
    void addChar(Character* character);

    /*
     * Nome: removeChar
     * Descricao: Remove um personagem do time
     * Entrada: (int) Posicao do personagem no vetor
     * Saida: (void)
    */
    void removeChar(int pos);

    /*
     * Nome: removeChar
     * Descricao: Remove um personagem do time
     * Entrada: (Character*) Personagem que sera removido
     * Saida: (void)
    */
    void removeChar(Character* character);

    /*
     * Nome: searchChar
     * Descricao: Retorna um personagem do time
     * Entrada: (std::string) Nome do personagem
     * Saida: (Character*) Personagem com o nome procurado
    */
    Character* searchChar(std::string characterName);

    /*
     * Nome: searchChar
     * Descricao: Retorna um personagem do time
     * Entrada: (int) Posicao do personagem
     * Saida: (Character*) Personagem na posicao procurada
    */
    Character* searchChar(int characterpos);
    
    /*
     * Nome: getPoints
     * Descricao: Pontuacao do time, dada pela media do HP dos personagens 
     * Saida: (double) Pontuacao
    */
    double getPoints();
};

#endif
