#ifndef _WEAPON_HPP_
#define _WEAPON_HPP_

#include <string>

#include "Item.hpp"

/*
 * Classe Weapon
 *  Representa uma arma no jogo
*/
class Weapon : public Item
{
    protected:
    int attackpts;

    double range;

    public:
    /*
     * Nome: Weapon (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (std::string) nome da arma, (double) preço, (int) pontos de ataque, (double) alcance
    */
    Weapon(std::string name, double price, int attackpts, double range);

    /*
     * Nome: Weapon (Construtor de cópia)
     * Descricao: Construtor que copia os atributos de um objeto Weapon
     * Entrada: (Weapon&) Referencia para um objeto Weapon
    */
    Weapon(Weapon & weapon);

    /*
     * Nome: getDefensePts
     * Descricao: retorna os pontos de defesa, nao usado nessa classe
     * Saida: (int) 0
    */
    int getDefensePts();

    /*
     * Nome: getRange
     * Descricao: retorna o alcance da arma
     * Saida: (double) range
    */
    double getRange();

    /*
     * Nome: getAttackPts
     * Descricao: retorna os pontos de ataque da arma
     * Saida: (int) pontos de ataque
    */
    int getAttackPts();
};

#endif
