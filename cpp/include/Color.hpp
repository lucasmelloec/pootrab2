#ifndef _COLOR_HPP_
#define _COLOR_HPP_

enum Color
{
    blue,
    red,
    green,
    yellow,
    white,
    black
};

#endif
