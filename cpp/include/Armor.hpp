#ifndef _ARMOR_HPP_
#define _ARMOR_HPP_

#include <string>

#include "Item.hpp"

/*
 * Classe Armor
 *  Representa uma armadura no jogo
*/
class Armor : public Item
{
    protected:
    int defensepts;

    double weight;

    public:
    /*
     * Nome: Armor (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (std::string) nome da armadura, (double) preço, (int) pontos de defesa, (double) peso
    */
    Armor(std::string name, double price, int defensepts, double weight);

    /*
     * Nome: Armor (Construtor de cópia)
     * Descricao: Construtor que copia os atributos de um objeto Armor
     * Entrada: (Armor&) Referencia para um objeto Armor
    */
    Armor(Armor & armor);

    /*
     * Nome: getDefensePts
     * Descricao: retorna os pontos de defesa da armadura
     * Saida: (int) pontos de defesa
    */
    int getDefensePts();
    
    /*
     * Nome: getAttackPts
     * Descricao: retorna os pontos de ataque da armadura, nao usado nessa classe
     * Saida: (int) 0
    */
    int getAttackPts();

    /*
     * Nome: getWeight
     * Descricao: retorna o peso da armadura
     * Saida: (double) peso
    */
    double getWeight();
};

#endif
