#ifndef _WIZARD_HPP_
#define _WIZARD_HPP_

#include <string>

#include "Character.hpp"

/*
 * Classe Wizard
 *     Tipo de personagem (mago)
*/
class Wizard : public Character
{
    protected:
    int wisdom; // sabedoria

    public:
    /*
     * Nome: Wizard (Construtor)
     * Descricao: Construtor que inicia os atributos
     * Entrada: (std::string) nome do personagem, (int) sabedoria
    */
    Wizard(std::string alias, int wisdom);

    /*
     * Nome: Wizard (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (std::string) nome do personagem, (int) sabedoria, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    Wizard(std::string alias, int wisdom, int strength, int speed, int dexterity, int constitution, int invSpace);

    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    int getAttackPoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    int getDefensePoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: attack
     * Descricao: ataca um outro personagem
     * Entrada: (Character*) Referencia a outro personagem
     * Saida: (void)
    */
    void attack(Character* character);

    /*
     * Nome: addWisdom
     * Descricao: Adiciona sabedoria
     * Entrada: (int) sabedoria a ser adicionada(ou removido se for negativo)
     * Saida: (void)
    */
    void addWisdom(int wisdom);
};

#endif
