#ifndef _ITEM_HPP
#define _ITEM_HPP

#include <string>

/*
 * Classe Character
 *  Utilizada apenas como referencia para o metodo use
*/
class Character;

/*
 * Classe Item
 *  Representa um item no jogo
*/
class Item
{
    private:
    std::string name; // Nome do item
    double price; // Preço do item

    public:
    /*
     * Nome: Item (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (std::string) nome do item, (double) preço
    */
    Item(std::string name, double price);

    /*
     * Nome: Item (Construtor de copia)
     * Descricao: Construtor que copia os atributos de um objeto Item
     * Entrada: (Item&) referencia ao objeto Item
    */
    Item(Item & item);

    /*
     * Nome: getName
     * Descricao: Retorna o nome do item
     * Saida: (std::string) nome
    */
    std::string getName();

    /*
     * Nome: getPrice
     * Descricao: Retorna o preço do item
     * Saida: (double) preço
    */
    double getPrice();

    /*
     * Nome: use
     * Descricao: metodo virtual, utiliza um item
     * Entrada: (Character*) personagem usando a pocao
     * Saida: (void)
    */
    virtual void use(Character* character);

    /*
     * Nome: getDefensePts
     * Descricao: metodo puramente virtual, retorna os pontos de defesa do item
     * Saida: (int) Pontos de defesa
    */
    virtual int getDefensePts() = 0;

    /*
     * Nome: getAttackPts
     * Descricao: metodo puramente virtual, retorna os pontos de ataque do item
     * Saida: (int) Pontos de ataque
    */
    virtual int getAttackPts() = 0;
    
    /*
     * Nome: getWeight
     * Descricao: Metodo virutal, retorna o peso do item
     * Saida: (double) peso
    */
    double getWeight();
};

#endif
