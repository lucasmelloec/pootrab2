#ifndef _POTION_HPP_
#define _POTION_HPP_

#include <string>

#include "Item.hpp"

/*
 * Classe Potion
 *  Representa uma poção no jogo
*/
class Potion : public Item
{
    private:
    int restorepts;

    public:
    /*
     * Nome: Potion (Construtor)
     * Descricao: Construtor que define os valores iniciais para os atributos da classe
     * Entrada: (std::string) nome da pocao, (double) preço, (int) poder de restauracao
    */
    Potion(std::string name, double price, int restorepts);

    /*
     * Nome: Potion (Construtor de cópia)
     * Descricao: Construtor que copia os atributos de um objeto Potion
     * Entrada: (Potion&) Referencia para um objeto Potion
    */
    Potion(Potion & potion);

    /*
     * Nome: ~Potion (Destrutor)
     * Descricao: Destrutor da classe
    */
    virtual ~Potion();

    /*
     * Nome: getDefensePts
     * Descricao: retorna os pontos de restauracao da pocao
     * Saida: (int) Pontos de restauracao
    */
    int getDefensePts();

    /*
     * Nome: getAttackPts
     * Descricao: retorna os pontos de ataque do item, nao usado nessa classe
     * Saida: (int) 0
    */
    int getAttackPts();

    /*
     * Nome: use
     * Descricao: método puramente virtual, usa a pocao
     * Entrada: (Character*) personagem usando a pocao
     * Saida: (void)
    */
    virtual void use(Character* character) = 0;
};

#endif
