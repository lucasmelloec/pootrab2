#ifndef _CHARACTER_HPP_
#define _CHARACTER_HPP_

#include <string>

#include "Inventory.hpp"

class Character
{
    private:
    std::string alias; // Nome do personagem

    Inventory myitems; // Inventario do personagem

    int HP; // Pontos de vida
    int MP; // Pontos de magia
    
    private:
    int numWeapons; // Quantas armas estao equipadas
    int numArmors; // Quantas armaduras estao equipadas

    protected:
    int XP; // Experiencia
    int strength; // Força
    int speed; // Velocidade
    int dexterity; // Destreza
    int constitution; // Constituição

    public:
    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos e recebe apenas alias
     * Entrada: (std::string) nome do personagem
    */
    Character(std::string alias);

    /*
     * Nome: Character (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (std::string) nome do personagem, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    Character(std::string alias, int strength, int speed, int dexterity, int constitution, int invSpace);

    /*
     * Nome: getName
     * Descricao: Retorna o alias
     * Saida: (std::string) nome do personagem
    */
    std::string getName();

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    virtual int getDefensePoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    virtual int getAttackPoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: attack
     * Descricao: Metodo puramente virtual, ataca um outro personagem
     * Entrada: (Character*) Referencia a outro personagem
     * Saida: (void)
    */
    virtual void attack(Character* character) = 0;

    /*
     * Nome: addXP
     * Descricao: Adiciona XP
     * Entrada: (int) xp a ser adicionada
     * Saida: (void)
    */
    void addXP(int XP);

    /*
     * Nome: setStrength
     * Descricao: Atribui um valor de força
     * Entrada: (int) força
     * Saida: (void)
    */
    void setStrength(int strength);

    /*
     * Nome: setSpeed
     * Descricao: Atribui um valor de velocidade
     * Entrada: (int) velocidade
     * Saida: (void)
    */
    void setSpeed(int speed);

    /*
     * Nome: setDexterity
     * Descricao: Atribui um valor de dextreza
     * Entrada: (int) dextreza
     * Saida: (void)
    */
    void setDexterity(int dexterity);

    /*
     * Nome: setConstitution
     * Descricao: Atribui um valor de constituicao
     * Entrada: (int) constituicao
     * Saida: (void)
    */
    void setConstitution(int constitution);

    /*
     * Nome: addHP
     * Descricao: Adiciona um valor de HP
     * Entrada: (int) HP a ser adicionada
     * Saida: (void)
    */
    void addHP(int HP);

    /*
     * Nome: addMP
     * Descricao: Adiciona um valor de MP
     * Entrada: (int) MP a ser adicionada
     * Saida: (void)
    */
    void addMP(int MP);

    /*
     * Nome: getSpeed
     * Descricao: Retorna a velocidade modificada pela armadura
     * Saida: (int) velocidade modificada pelo peso da armadura
    */
    int getSpeed();
    
    /*
     * Nome: getHP
     * Descricao: Retorna o HP do personagem
     * Saida: (int) HP
    */
    int getHP();
    
    /*
     * Nome: insertItem
     * Descricao: Coloca um item no inventario
     * Entrada: (Item*) item
     * Saida: (void)
    */
    void insertItem(Item* item);

    /*
     * Nome: removeItem
     * Descricao: Tira um item no inventario
     * Entrada: (std::string) nome do item
     * Saida: (void)
    */
    void removeItem(std::string itemName);
    
    /*
     * Nome: equipWeapon
     * Descricao: Equipa uma arma do inventario
     * Entrada: (std::string) nome da arma
     * Saida: (void)
    */
    void equipWeapon(std::string weaponName);
    
    /*
     * Nome: equipArmor
     * Descricao: Equipa uma armadura do inventario
     * Entrada: (std::string) nome da armadura
     * Saida: (void)
    */
    void equipArmor(std::string armorName);
    
    /*
     * Nome: unequipWeapon
     * Descricao: Desequipa uma arma
     * Entrada: (std::string) nome da arma
     * Saida: (void)
    */
    void unequipWeapon(std::string weaponName);
    
    /*
     * Nome: unequipArmor
     * Descricao: Desequipa uma armadura
     * Entrada: (std::string) nome da armadura
     * Saida: (void)
    */
    void unequipArmor(std::string armorName);
};

#endif
