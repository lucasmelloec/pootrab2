#ifndef _THIEF_HPP_
#define _THIEF_HPP_

#include <string>

#include "Character.hpp"

/*
 * Classe Thief
 *     Tipo de personagem (ladino)
*/
class Thief : public Character
{
    protected:
    int stealth;

    public:
    /*
     * Nome: Thief (Construtor)
     * Descricao: Construtor que inicia os atributos
     * Entrada: (std::string) nome do personagem, (int) stealth
    */
    Thief(std::string alias, int stealth);
    
    /*
     * Nome: Thief (Construtor)
     * Descricao: Construtor que inicia os atributos a partir de valores dados
     * Entrada: (std::string) nome do personagem, (int) stealth, (int) strength, (int) speed,
     * (int) dexterity, (int) constitution, (int) espaco no inventario
    */
    Thief(std::string alias, int stealth, int strength, int speed, int dexterity, int constitution, int invSpace);

    /*
     * Nome: getAttackPoints
     * Descricao: Retorna a quantidade de pontos de ataque
     * Saida: (int) Pontos de Ataque
    */
    int getAttackPoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: getDefensePoints
     * Descricao: Retorna a quantidade de pontos de defesa
     * Saida: (int) Pontos de Defesa
    */
    int getDefensePoints(); // Alterado para public, para ser chamado na funcao ataque por uma instancia de Character corretamente

    /*
     * Nome: attack
     * Descricao: ataca um outro personagem
     * Entrada: (Character*) Referencia a outro personagem
     * Saida: (void)
    */
    void attack(Character* character);

    /*
     * Nome: addStealth
     * Descricao: Adiciona stealth
     * Entrada: (int) stealth a ser adicionada(ou removido se for negativo)
     * Saida: (void)
    */
    void addStealth(int stealth);
};

#endif
