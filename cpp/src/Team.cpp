#include <sstream>

#include "Team.hpp"

/*
 * Nome: Team (Construtor)
 * Descricao: Construtor que inicia os atributos
 * Entrada: (std::string) nome do personagem, (Color) cor do time
*/
Team::Team(std::string name, Color color)
{
    this->name = name;
    this->color = color;

    win = 0;
    lose = 0;
    draw = 0;
}

/*
 * Nome: ~Team (Destrutor)
 * Descricao: Destrutor que elimina todos os personagens
*/
Team::~Team()
{
    for(std::vector<Character*>::iterator it = characters.begin(); it != characters.end(); it++)
        delete (*it);
}

/*
 * Nome: getName
 * Descricao: Retorna o nome
 * Saida: (std::string) nome do personagem
*/
std::string Team::getName()
{
    return name;
}

/*
 * Nome: getResults
 * Descricao: Retorna os resultados do time
 * Saida: (std::string) resultados do time
*/
std::string Team::getResults()
{
    std::stringstream ss;
    ss << "Vitorias: " << win << ".  Derrotas: " << lose << ".  Empates: " << draw << ".";

    return ss.str();
}

/*
 * Nome: toString
 * Descricao: Retorna o nome e a cor do time
 * Saida: (std::string) Nome e cor do time
*/
std::string Team::toString()
{
    std::stringstream ss;
    std::string cor;

    switch(color)
    {
        case blue:
            cor = "blue";
            break;
        case red:
            cor = "red";
            break;
        case green:
            cor = "green";
            break;
        case yellow:
            cor = "yellow";
            break;
        case white:
            cor = "white";
            break;
        case black:
            cor = "black";
            break;
        default:
            cor = "no color";
    }

    ss << "Nome do Time: " << name << ".  Cor do Time: " << cor << ".";

    return ss.str();
}

/*
 * Nome: resolveBattle
 * Descricao: Resolve uma batalha com um outro time, verificando os pontos e adicionando uma vitoria, derrota ou empate
 * Entrada: (Team&) Time com o qual a batalha deve ser resolvida
 * Saida: (int) -1 se o time perdeu, 0 se empatou e 1 se ganhou
*/
int Team::resolveBattle(Team & teamB)
{
    double pointsA = getPoints();
    double pointsB = teamB.getPoints();

    // Time A ganhou
    if(pointsA > pointsB)
    {
        win++;
        return 1;
    }
    // Time A perdeu
    else if(pointsA < pointsB)
    {
        lose++;
        return -1;
    }

    // Caso de empate
    draw++;
    return 0;
}

/*
 * Nome: addChar
 * Descricao: Adiciona um personagem ao time
 * Entrada: (Character*) Personagem que sera adicionado
 * Saida: (void)
*/
void Team::addChar(Character* character)
{
    characters.push_back(character);
}

/*
 * Nome: removeChar
 * Descricao: Remove um personagem do time
 * Entrada: (int) Posicao do personagem no vetor
 * Saida: (void)
*/
void Team::removeChar(int pos)
{
    // Se a posicao estiver abaixo do limite permitido
    if(pos < characters.size())
        characters.erase(characters.begin() + pos);
}

/*
 * Nome: removeChar
 * Descricao: Remove um personagem do time
 * Entrada: (Character*) Personagem que sera removido
 * Saida: (void)
*/
void Team::removeChar(Character* character)
{
    // Itera por todos os personagens
    for(std::vector<Character*>::iterator it = characters.begin(); it != characters.end(); it++)
        if( (*it) == character) // Se o personagem for o mesmo que esta sendo procurado
        {
            characters.erase(it); // Retorna o personagem
            it = characters.end();
        }
}

/*
 * Nome: searchChar
 * Descricao: Retorna um personagem do time
 * Entrada: (std::string) Nome do personagem
 * Saida: (Character*) Personagem com o nome procurado
*/
Character* Team::searchChar(std::string characterName)
{
    // Itera por todos os personagens
    for(std::vector<Character*>::iterator it = characters.begin(); it != characters.end(); it++)
        if( (*it)->getName().compare(characterName) == 0) // Se nome do personagem for o mesmo que esta sendo procurado
            return (*it); // Retorna o personagem

    return NULL; // Retorna NULL caso o personagem nao seja encontrado
}

/*
 * Nome: searchChar
 * Descricao: Retorna um personagem do time
 * Entrada: (int) Posicao do personagem
 * Saida: (Character*) Personagem na posicao procurada
*/
Character* Team::searchChar(int characterpos)
{
    return characters.at(characterpos);
}

/*
 * Nome: getPoints
 * Descricao: Pontuacao do time, dada pela media do HP dos personagens 
 * Saida: (double) Pontuacao
*/
double Team::getPoints()
{
    double points = 0;

    // Itera por todos os personagens
    for(std::vector<Character*>::iterator it = characters.begin(); it != characters.end(); it++)
        points += (*it)->getHP();

    points /= (double)characters.size();

    return points;
}
