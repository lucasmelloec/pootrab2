#include <iostream>

#include "Armor.hpp"

/*
 * Nome: Armor (Construtor)
 * Descricao: Construtor que define os valores iniciais para os atributos da classe
 * Entrada: (std::string) nome da armadura, (double) preço, (int) pontos de defesa, (double) peso
*/
Armor::Armor(std::string name, double price, int defensepts, double weight) : Item(name, price)
{
    // Se os pontos de defesa estiverem dentro do intervalo permitido
    if(defensepts >= 1 && defensepts <= 20) 
    {
        this->defensepts = defensepts;
    }
    else
    {
        this->defensepts = 1; // Atribui valor padrao
        std::cerr << "Erro em Armor::Armor(std::string, double, int, int): defensepts deve estar entre 1 e 20." << std::endl;
    }

    if(weight >= 1 && weight <= 20)
    {
        this->weight = weight;
    }
    else
    {
        this->weight = 1; // Atribui valor padrao
        std::cerr << "Erro em Armor::Amor(std::string, double, int, int) : weight deve estar entre 1 e 20." << std::endl;
    }
}

/*
 * Nome: Armor (Construtor de cópia)
 * Descricao: Construtor que copia os atributos de um objeto Armor
 * Entrada: (Armor&) Referencia para um objeto Armor
*/
Armor::Armor(Armor & armor) : Item(armor)
{
    defensepts = armor.defensepts;
    weight = armor.weight;
}

/*
 * Nome: getDefensePts
 * Descricao: retorna os pontos de defesa da armadura
 * Saida: (int) pontos de defesa
*/
int Armor::getDefensePts()
{
    return defensepts;
}

/*
 * Nome: getAttackPts
 * Descricao: retorna os pontos de ataque da armadura, nao usado nessa classe
 * Saida: (int) 0
*/
int Armor::getAttackPts()
{
    return 0;
}

/*
 * Nome: getWeight
 * Descricao: retorna o peso da armadura
 * Saida: (double) peso
*/
double Armor::getWeight()
{
    return weight;
}
