#include <iostream>

#include "Inventory.hpp"

/*
 * Nome: Inventory (Construtor)
 * Descricao: Construtor que inicializa os atributos em 0 e sem items no vetor
*/
Inventory::Inventory()
{
    spaces = 0;
    gold = 0;
}

/*
 * Nome: Inventory (Destrutor)
 * Descricao: Destrutor que elimina todos os itens em Inventory
*/
Inventory::~Inventory()
{
    for(std::vector<std::pair<Item*, bool>>::iterator it = items.begin(); it != items.end(); it++)
        delete (*it).first;
}

/*
 * Nome: getTotalGold
 * Descricao: Retorna a quantidade de ouro possuida pelo personagem
 * Saida: (double) quantia de ouro
*/
double Inventory::getTotalGold()
{
    return gold;
}

/*
 * Nome: getAvailableSpace
 * Descricao: Retorna quanto espaço vazio há no inventário
 * Saida: (int) espaco disponivel
*/
int Inventory::getAvailableSpace()
{
    return spaces - items.size();
}

/*
 * Nome: spendGold
 * Descricao: Diminui a quantia de ouro do personagem
 * Entrada: (double) quantia a ser retirada
 * Saida: (void)
*/
void Inventory::spendGold(double amount)
{
    if(amount <= gold)
        gold -= amount;
    else
        std::cerr << "Erro em Inventory::spendGold(double): o personagem nao possui ouro suficiente." << std::endl;
}

/*
 * Nome: earnGold
 * Descricao: Aumenta a quantia de ouro do personagem
 * Entrada: (double) quantia a ser acrescida
 * Saida: (void)
*/
void Inventory::earnGold(double amount)
{
    gold += amount;
}

/*
 * Nome: setSpaces
 * Descricao: Atribui uma quantidade de espacos no inventário
 * Entrada: (int) espaço disponivel
 * Saida: (void)
*/
void Inventory::setSpaces(int spaces)
{
    this->spaces = spaces;
}

/*
 * Nome: searchItem
 * Descricao: Retorna um item do inventario
 * Entrada: (std::string) nome do item
 * Saida: (Item*) referencia ao item com aquele nome
*/
Item* Inventory::searchItem(std::string itemName)
{
    // Itera por todos os itens
    for(std::vector<std::pair<Item*, bool>>::iterator it = items.begin(); it != items.end(); it++)
        if( (*it).first->getName().compare(itemName) == 0 ) // Se nome do item for o mesmo que esta sendo procurado
            return (*it).first; // Retorna o item

    return NULL;    // Retorna NULL caso o item nao seja encontrado
}

/*
 * Nome: searchItem
 * Descricao: Retorna um item do inventario
 * Entrada: (int) posicao do item
 * Saida: (Item*) referencia ao item com aquela posicao no vetor
*/
Item* Inventory::searchItem(int pos)
{
    // Se a posicao estiver abaixo do limite permitido
    if(pos < items.size())
        return items.at(pos).first;

    return NULL; // Se a posicao do item ultrapassa o limite, retorna NULL
}

/*
 * Nome: insertItem
 * Descricao: Insere um item no inventário
 * Entrada: (Item*) referencia ao item
 * Saida: (void)
*/
void Inventory::insertItem(Item* item)
{
    // Somente insere se houver espaco disponivel no inventario
    if(items.size() < spaces)
        items.push_back(std::pair<Item*, bool>(item, false));
    else
        std::cerr << "Erro em Inventory::insertItem: O personagem nao possui espaco disponivel no inventario." << std::endl;
}

/*
 * Nome: removeItem
 * Descricao: Remove um item do inventário
 * Entrada: (std::string) nome do item
 * Saida: (void)
*/
void Inventory::removeItem(std::string itemName)
{
    // Itera por todos os itens
    for(std::vector<std::pair<Item*, bool>>::iterator it = items.begin(); it != items.end(); it++)
    {
        if( (*it).first->getName().compare(itemName) == 0 ) // Se nome do item for o mesmo que esta sendo procurado
        {
            // Tira o item do vetor
            items.erase(it); // Tira do vetor
            return; // Sai da funcao
        }
    }
}

/*
 * Nome: removeItem
 * Descricao: Remove um item do inventário
 * Entrada: (int) posicao do item
 * Saida: (void)
*/
void Inventory::removeItem(int pos)
{
    if(pos < items.size())
        items.erase(items.begin() + pos);
    else
        std::cerr << "Erro em Inventory.removeItem: A posicao nao correponde a um item valido." << std::endl;
}

/*
 * Nome: getNumberOfItems
 * Descricao: Retorna quantos itens existem no inventario
 * Saida: (int) quantia de itens
*/
int Inventory::getNumberOfItems()
{
    return items.size();
}

/*
 * Nome: equipItem
 * Descricao: Equipa um item do inventário
 * Entrada: (std::string) nome do item
 * Saida: (void)
*/
void Inventory::equipItem(std::string itemName)
{
    // Itera por todos os itens
    for(std::vector<std::pair<Item*, bool>>::iterator it = items.begin(); it != items.end(); it++)
    {
        if( (*it).first->getName().compare(itemName) == 0 && (*it).second == false) // Se nome do item for o mesmo que esta sendo procurado
        {
            (*it).second = true; // Seta para equipado

            return; // Sai da funcao
        }
    }
}

/*
 * Nome: unequipItem
 * Descricao: Desequipa um item do inventário
 * Entrada: (std::string) nome do item
 * Saida: (void)
*/
void Inventory::unequipItem(std::string itemName)
{
    // Itera por todos os itens
    for(std::vector<std::pair<Item*, bool>>::iterator it = items.begin(); it != items.end(); it++)
    {
        if( (*it).first->getName().compare(itemName) == 0 && (*it).second == true ) // Se nome do item for o mesmo que esta sendo procurado
        {
            (*it).second = false; // Seta para desequipado

            return; // Sai da funcao
        }
    }
}

/*
 * Nome: isEquipped
 * Descricao: Retorna de um item esta equipado
 * Entrada: (int) posicao do item
 * Saida: (bool) esta equipado?
*/
bool Inventory::isEquipped(int itemPos)
{
    return items.at(itemPos).second; // Retorna se esta equipado ou nao
}
