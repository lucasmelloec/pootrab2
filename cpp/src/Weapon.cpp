#include <iostream>

#include "Weapon.hpp"

/*
 * Nome: Weapon (Construtor)
 * Descricao: Construtor que define os valores iniciais para os atributos da classe
 * Entrada: (std::string) nome da arma, (double) preço, (int) pontos de ataque, (double) alcance
*/
Weapon::Weapon(std::string name, double price, int attackpts, double range) : Item(name, price)
{
    // Se os pontos de ataque estiverem dentro do intervalo permitido
    if(attackpts >= 1 && attackpts <= 9) 
    {
        this->attackpts = attackpts;
    }
    else
    {
        this->attackpts = 1; // Atribui valor padrao
        std::cerr << "Erro em Weapon::Weapon(std::string, double, int, int): attackpts deve estar entre 1 e 9." << std::endl;
    }

    this->range = range;
}

/*
 * Nome: Weapon (Construtor de cópia)
 * Descricao: Construtor que copia os atributos de um objeto Weapon
 * Entrada: (Weapon&) Referencia para um objeto Weapon
*/
Weapon::Weapon(Weapon & weapon) : Item(weapon)
{
    attackpts = weapon.attackpts;
    range = weapon.range;
}

/*
 * Nome: getDefensePts
 * Descricao: retorna os pontos de defesa, nao usado nessa classe
 * Saida: (int) 0
*/
int Weapon::getDefensePts()
{
    return 0;
}

/*
 * Nome: getRange
 * Descricao: retorna o alcance da arma
 * Saida: (double) range
*/
double Weapon::getRange()
{
    return range;
}

/*
 * Nome: getAttackPts
 * Descricao: retorna os pontos de ataque da arma
 * Saida: (int) pontos de ataque
*/
int Weapon::getAttackPts()
{
    return attackpts;
}
