#include <cstdlib>
#include <iostream>

#include "Knight.hpp"
#include "Thief.hpp"
#include "Wizard.hpp"
#include "HealthPotion.hpp"
#include "ManaPotion.hpp"
#include "Armor.hpp"
#include "Weapon.hpp"
#include "Team.hpp"

int main(int argc, char* argv[])
{
    // Atribui um seed para os valores randomicos
    srand(time(NULL));

    // Instancia os personagens
    Character* garen= new Knight("Garen", 5, 30, 20, 10, 40, 6);
    Character* kyle= new Knight("Kyle", 4, 30, 30, 10, 30, 6);
    Character* jarvan= new Knight("Jarvan", 3, 40, 20, 10, 30, 6);
    Character* morgana= new Wizard("Morgana", 3, 10, 30, 40, 20, 6);
    Character* lux= new Wizard("Lux", 3, 10, 40, 40, 10, 6);
    Character* leblanc= new Wizard("LeBlanc", 4, 15, 35, 40, 10, 6);
    Character* khazix= new Thief("Khazix", 3, 30, 50, 15, 5, 6);
    Character* twitch= new Thief("Twitch", 3, 40, 20, 30, 10, 6);

    // Instancia dois times
    Team dire("Dire", black);
    Team radiant("Radiant", white);

    // Instancia os itens
    Armor* iron_armor = new Armor("Armadura de Ferro", 0, 5, 20.0);
    Armor* leather_armor = new Armor("Armadura de Couro", 0, 2, 12.0);
    Armor* steel_armor = new Armor("Armadura de Aco", 0, 6, 16.0);
    Armor* brass_armor = new Armor("Armadura de Bronze", 0, 4, 18.0);
    Armor* gold_armor = new Armor("Armadura de Ouro", 0, 3, 18.0);
    Armor* mithril_armor = new Armor("Armadura de Mithril", 0, 8, 12.0);
    
    Weapon* short_bow = new Weapon("Arco Curto", 0, 3, 3);
    Weapon* long_bow = new Weapon("Arco Longo", 0, 5, 4);
    Weapon* short_sword = new Weapon("Espada Curta", 0, 4, 4);
    Weapon* long_sword = new Weapon("Espada Longa", 0, 5, 5);
    Weapon* gun = new Weapon("Arma", 0, 5, 1);
    Weapon* air_staff = new Weapon("Cajado de Ar", 0, 4, 2);
    Weapon* water_staff = new Weapon("Cajado de Agua", 0, 4, 2);
    Weapon* crossbow = new Weapon("Besta", 0, 5, 3);
    Weapon* hammer = new Weapon("Martelo", 0, 6, 6);
    Weapon* axe = new Weapon("Machado", 0, 4, 5);

    HealthPotion* smallHealth1 = new HealthPotion("Pocao pequena de vida", 0, 10);
    HealthPotion* smallHealth2 = new HealthPotion("Pocao pequena de vida", 0, 10);
    HealthPotion* smallHealth3 = new HealthPotion("Pocao pequena de vida", 0, 10);
    HealthPotion* greatHealth1 = new HealthPotion("Pocao grande de vida", 0, 30);
    HealthPotion* greatHealth2 = new HealthPotion("Pocao grande de vida", 0, 30);
    
    ManaPotion* smallMana1 = new ManaPotion("Pocao pequena de mana", 0, 10);
    ManaPotion* smallMana2 = new ManaPotion("Pocao pequena de mana", 0, 10);
    ManaPotion* smallMana3 = new ManaPotion("Pocao pequena de mana", 0, 10);
    
    // Equipa os itens
    garen->insertItem(iron_armor);
    kyle->insertItem(gold_armor);
    jarvan->insertItem(steel_armor);
    leblanc->insertItem(leather_armor);
    khazix->insertItem(mithril_armor);
    twitch->insertItem(brass_armor);
    
    garen->equipArmor(iron_armor->getName());
    kyle->equipArmor(gold_armor->getName());
    jarvan->equipArmor(steel_armor->getName());
    leblanc->equipArmor(leather_armor->getName());
    khazix->equipArmor(mithril_armor->getName());
    twitch->equipArmor(brass_armor->getName());

    garen->insertItem(long_sword);
    kyle->insertItem(hammer);
    jarvan->insertItem(short_sword);
    jarvan->insertItem(axe);
    morgana->insertItem(short_bow);
    lux->insertItem(water_staff);
    leblanc->insertItem(air_staff);
    khazix->insertItem(long_bow);
    twitch->insertItem(gun);
    twitch->insertItem(crossbow);
    
    garen->equipWeapon(long_sword->getName());
    kyle->equipWeapon(hammer->getName());
    jarvan->equipWeapon(short_sword->getName());
    jarvan->equipWeapon(axe->getName());
    morgana->equipWeapon(short_bow->getName());
    lux->equipWeapon(water_staff->getName());
    leblanc->equipWeapon(air_staff->getName());
    khazix->equipWeapon(long_bow->getName());
    twitch->equipWeapon(gun->getName());
    twitch->equipWeapon(crossbow->getName());
    
    garen->insertItem(smallHealth1);
    kyle->insertItem(smallHealth2);
    jarvan->insertItem(smallHealth3);
    khazix->insertItem(greatHealth1);
    twitch->insertItem(greatHealth2);
    
    morgana->insertItem(smallMana1);
    lux->insertItem(smallMana2);
    leblanc->insertItem(smallMana3);
    
    // Formacao dos times
    dire.addChar(morgana);
    dire.addChar(twitch);
    dire.addChar(khazix);
    dire.addChar(leblanc);
    
    radiant.addChar(garen);
    radiant.addChar(jarvan);
    radiant.addChar(lux);
    radiant.addChar(kyle);

    // Uma rodada de batalha
    std::vector<Character*> DireAttackers;
    std::vector<Character*> RadiantAttackers;

    for(int i = 0; i < 4; i++)
    {
        DireAttackers.push_back(dire.searchChar(i));
        RadiantAttackers.push_back(radiant.searchChar(i));
    }

    Character* firstAttackerRadiant = NULL;
    Character* firstAttackerDire = NULL;

    int teamTurn = rand() % 2; // Time que fará o combate. Se o numero for par, são os Dire, se for impar sao os Radiant

    while(DireAttackers.size() > 0 || RadiantAttackers.size() > 0) // Enquanto houver personagens na lista de ataque
    {
        // Se o valor for par, os Dire fazem o ataque
        if(teamTurn % 2 == 0)
        {
            int attacker = rand() % DireAttackers.size(); // Decide qual personagem do time atual é o atacante

            if(teamTurn < 2) // Se é o primeiro turno desse grupo
                firstAttackerDire = DireAttackers.at(attacker); // Salva o primeiro atacante do time

            if(RadiantAttackers.size() == 0) // Se nao ha mais personagens do outro time para atacar
                DireAttackers.at(attacker)->attack(firstAttackerRadiant); // Ataca o primeiro personagem atacante do outro time
            else
            {
                int defender = rand() % RadiantAttackers.size(); // Decide qual personagem do outro time sera atacado

                DireAttackers.at(attacker)->attack(RadiantAttackers.at(defender)); // Realiza o ataque
            }

            DireAttackers.erase(DireAttackers.begin() + attacker); // Remove o personagem da lista de atacantes, pois ele nao atacara novamente nessa rodada

            teamTurn++; // Turno do proximo time
        }
        // Se o valor for impar, os Radiant fazem o ataque
        else
        {
            int attacker = rand() % RadiantAttackers.size(); // Decide qual personagem do time atual é o atacante

            if(teamTurn < 2) // Se é o primeiro turno desse grupo
                firstAttackerRadiant = RadiantAttackers.at(attacker); // Salva o primeiro atacante do time
            
            if(DireAttackers.size() == 0) // Se nao ha mais personagens do outro time para atacar
                RadiantAttackers.at(attacker)->attack(firstAttackerDire); // Ataca o primeiro personagem atacante do outro time
            else
            {
                int defender = rand() % DireAttackers.size(); // Decide qual personagem do outro time sera atacado

                RadiantAttackers.at(attacker)->attack(DireAttackers.at(defender)); // Realiza o ataque
            }

            RadiantAttackers.erase(RadiantAttackers.begin() + attacker); // Remove o personagem da lista de atacantes, pois ele nao atacara novamente nessa rodada

            teamTurn++; // Turno do proximo time
        }
   
    }

    dire.resolveBattle(radiant);
    radiant.resolveBattle(dire);

    std::cout << dire.toString() << " Obteve os seguintes resultados: " << dire.getResults() << std::endl;
    std::cout << radiant.toString() << " Obteve os seguintes resultados: " << radiant.getResults() << std::endl;
}
