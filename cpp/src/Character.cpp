#include <iostream>
#include <cmath>

#include "Character.hpp"

/*
 * Nome: Character (Construtor)
 * Descricao: Construtor que inicia os atributos e recebe apenas alias
 * Entrada: (std::string) nome do personagem
*/
Character::Character(std::string alias)
{
    HP = 100; // Valor inicial de HP
    MP = 0; // Valor inicial de MP

    strength = speed = dexterity = constitution = 25; // Valores padroes de atributos

    XP = 1; // Valor inicial de XP

    this->alias = alias;

    myitems.setSpaces(6); // Quantidade padrao de espacos no inventario
    
    numWeapons = 0;
    numArmors = 0;
}

/*
 * Nome: Character (Construtor)
 * Descricao: Construtor que inicia os atributos a partir de valores dados
 * Entrada: (std::string) nome do personagem, (int) strength, (int) speed,
 * (int) dexterity, (int) constitution, (int) espaco no inventario
*/
Character::Character(std::string alias, int strength, int speed, int dexterity, int constitution, int invSpace)
{
    HP = 100; // Valor inicial de HP
    MP = 0; // Valor inicial de MP

    this->strength = strength;
    this->speed = speed;
    this->dexterity = dexterity;
    this->constitution = constitution;

    if(strength + speed + dexterity + constitution != 100) // Se os atributos nao somarem 100
        strength = speed = dexterity = constitution = 25; // entao atribui os valores padroes

    if(strength < 1 || speed < 1 || dexterity < 1 || constitution < 1) // Se algum atributo tiver abaixo do valor minimo
        strength = speed = dexterity = constitution = 25; // entao atribui os valores padroes

    XP = 1; // Valor inicial de XP

    this->alias = alias;

    myitems.setSpaces(invSpace); // Atribui a quantidade de espacos do inventario

    numWeapons = 0;
    numArmors = 0;
}

/*
 * Nome: getName
 * Descricao: Retorna o alias
 * Saida: (std::string) nome do personagem
*/
std::string Character::getName()
{
    return alias;
}

/*
 * Nome: getDefensePoints
 * Descricao: Retorna a quantidade de pontos de defesa
 * Saida: (int) Pontos de Defesa
*/
int Character::getDefensePoints()
{
    int item_def_pts = 0;

    for(int i = 0; i < myitems.getNumberOfItems(); i++)
        if(myitems.isEquipped(i) == true)
            item_def_pts += myitems.searchItem(i)->getDefensePts();

    double def_pts = ( ( constitution*0.5 + dexterity*0.3 + getSpeed()*0.2 ) + item_def_pts ) * (XP/2);

    return (double)def_pts;
}

/*
 * Nome: getAttackPoints
 * Descricao: Retorna a quantidade de pontos de ataque
 * Saida: (int) Pontos de Ataque
*/
int Character::getAttackPoints()
{
    int item_att_pts = 0;

    for(int i = 0; i < myitems.getNumberOfItems(); i++)
        if(myitems.isEquipped(i) == true)
            item_att_pts += myitems.searchItem(i)->getAttackPts();

    double att_pts = ( ( strength*0.5 + dexterity*0.3 + getSpeed()*0.2 ) + item_att_pts ) * (XP/3);

    return (double)att_pts;
}

/*
 * Nome: addXP
 * Descricao: Adiciona XP
 * Entrada: (int) xp a ser adicionada
 * Saida: (void)
*/
void Character::addXP(int XP)
{
    this->XP += XP;
    
    if(this->XP > 100)
        this->XP = 100;
    
    if(this->XP < 1)
        this->XP = 1;
}

/*
 * Nome: setStrength
 * Descricao: Atribui um valor de força
 * Entrada: (int) força
 * Saida: (void)
*/
void Character::setStrength(int strength)
{
    if(strength >= 1 && strength < 100)
        this->strength = strength;
}

/*
 * Nome: setSpeed
 * Descricao: Atribui um valor de velocidade
 * Entrada: (int) velocidade
 * Saida: (void)
*/
void Character::setSpeed(int speed)
{
    if(speed >= 1 && speed < 100)
        this->speed = speed;
}

/*
 * Nome: setDexterity
 * Descricao: Atribui um valor de dextreza
 * Entrada: (int) dextreza
 * Saida: (void)
*/
void Character::setDexterity(int dexterity)
{
    if(dexterity >= 1 && dexterity < 100)
        this->dexterity = dexterity;
}

/*
 * Nome: setConstitution
 * Descricao: Atribui um valor de constituicao
 * Entrada: (int) constituicao
 * Saida: (void)
*/
void Character::setConstitution(int constitution)
{
    if(constitution >= 1 && constitution < 100)
        this->constitution = constitution;
}

/*
 * Nome: addHP
 * Descricao: Adiciona um valor de HP
 * Entrada: (int) HP a ser adicionada
 * Saida: (void)
*/
void Character::addHP(int HP)
{
    this->HP += HP;

    if(this->HP > 100) // Valor maximo de HP
        this->HP = 100;

    if(this->HP <= 0)
        std::cout << alias << " morreu." << std::endl;
}

/*
 * Nome: addMP
 * Descricao: Adiciona um valor de MP
 * Entrada: (int) MP a ser adicionada
 * Saida: (void)
*/
void Character::addMP(int MP)
{
    this->MP += MP;

    if(this->MP > 100) // Valor maximo de MP
        this->MP = 100;

    if(this->MP < 0) // Valor minimo de MP
        this->MP = 0;
}

/*
 * Nome: getSpeed
 * Descricao: Retorna a velocidade modificada pela armadura
 * Saida: (int) velocidade modificada pelo peso da armadura
*/
int Character::getSpeed()
{
    double weight = 0;
    
    for(int i = 0; i < myitems.getNumberOfItems(); i++)
        if(myitems.isEquipped(i) == true)
            weight += myitems.searchItem(i)->getWeight();

    double modSpeed =  speed*exp(- (weight * weight)/20 );

    return (int)modSpeed;
}

/*
 * Nome: getHP
 * Descricao: Retorna o HP do personagem
 * Saida: (int) HP
*/
int Character::getHP()
{
    return HP;
}

/*
 * Nome: insertItem
 * Descricao: Coloca um item no inventario
 * Entrada: (Item*) item
 * Saida: (void)
*/
void Character::insertItem(Item* item)
{
    myitems.insertItem(item);
}

/*
 * Nome: removeItem
 * Descricao: Tira um item no inventario
 * Entrada: (std::string) nome do item
 * Saida: (void)
*/
void Character::removeItem(std::string itemName)
{
    myitems.removeItem(itemName);
}

/*
 * Nome: equipWeapon
 * Descricao: Equipa uma arma do inventario
 * Entrada: (std::string) nome da arma
 * Saida: (void)
*/
void Character::equipWeapon(std::string weaponName)
{
    if(numWeapons >= 2)
    {
        std::cerr << "Inventory.equipWeapon(std::string): Ja existem duas armas equipadas." << std::endl;
    }
    else
    {
        myitems.equipItem(weaponName);
        numWeapons++;
    }
}

/*
 * Nome: equipArmor
 * Descricao: Equipa uma armadura do inventario
 * Entrada: (std::string) nome da armadura
 * Saida: (void)
*/
void Character::equipArmor(std::string armorName)
{
    if(numArmors >= 1)
    {
        std::cerr << "Inventory.equipArmor(std::string): Ja existe uma armadura equipada." << std::endl;
    }
    else
    {
        myitems.equipItem(armorName);
        numArmors++;
    }
}

/*
 * Nome: unequipWeapon
 * Descricao: Desequipa uma arma
 * Entrada: (std::string) nome da arma
 * Saida: (void)
*/
void Character::unequipWeapon(std::string weaponName)
{
    if(numWeapons <= 0)
    {
        std::cerr << "Inventory.unequipWeapon(std::string): Nao existem armas equipadas." << std::endl;
    }
    else
    {
        myitems.unequipItem(weaponName);
        numWeapons--;
    }
}

/*
 * Nome: unequipArmor
 * Descricao: Desequipa uma armadura
 * Entrada: (std::string) nome da armadura
 * Saida: (void)
*/
void Character::unequipArmor(std::string armorName)
{
    if(numArmors <= 0)
    {
        std::cerr << "Inventory.unequipArmor(std::string): Nao existe armadura equipada." << std::endl;
    }
    else
    {
        myitems.unequipItem(armorName);
        numArmors--;
    }
}
