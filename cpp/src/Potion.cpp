#include "Potion.hpp"
#include "Character.hpp"

/*
 * Nome: Potion (Construtor)
 * Descricao: Construtor que define os valores iniciais para os atributos da classe
 * Entrada: (std::string) nome da pocao, (double) preço, (int) poder de restauracao
*/
Potion::Potion(std::string name, double price, int restorepts) : Item(name, price)
{
    this->restorepts = restorepts;
}

/*
 * Nome: Potion (Construtor de cópia)
 * Descricao: Construtor que copia os atributos de um objeto Potion
 * Entrada: (Potion&) Referencia para um objeto Potion
*/
Potion::Potion(Potion & potion) : Item(potion)
{
    restorepts = potion.restorepts;
}

/*
 * Nome: ~Potion (Destrutor)
 * Descricao: Destrutor da classe
*/
Potion::~Potion()
{
}

/*
 * Nome: getDefensePts
 * Descricao: retorna os pontos de restauracao da pocao
 * Saida: (int) Pontos de restauracao
*/
int Potion::getDefensePts()
{
    return restorepts;
}

/*
 * Nome: getAttackPts
 * Descricao: retorna os pontos de ataque do item, nao usado nessa classe
 * Saida: (int) 0
*/
int Potion::getAttackPts()
{
    return 0;
}
