#include <iostream>

#include "Wizard.hpp"

/*
 * Nome: Wizard (Construtor)
 * Descricao: Construtor que inicia os atributos
 * Entrada: (std::string) nome do personagem, (int) sabedoria
*/
Wizard::Wizard(std::string alias, int wisdom) : Character(alias)
{
    this->wisdom = wisdom;
}

/*
 * Nome: Wizard (Construtor)
 * Descricao: Construtor que inicia os atributos a partir de valores dados
 * Entrada: (std::string) nome do personagem, (int) sabedoria, (int) strength, (int) speed,
 * (int) dexterity, (int) constitution, (int) espaco no inventario
*/
Wizard::Wizard(std::string alias, int wisdom, int strength, int speed, int dexterity, int constitution, int invSpace) : Character(alias, strength, speed, dexterity, constitution, invSpace)
{
    this->wisdom = wisdom;
}

/*
 * Nome: getAttackPoints
 * Descricao: Retorna a quantidade de pontos de ataque
 * Saida: (int) Pontos de Ataque
*/
int Wizard::getAttackPoints()
{
    return Character::getAttackPoints();
}

/*
 * Nome: getDefensePoints
 * Descricao: Retorna a quantidade de pontos de defesa
 * Saida: (int) Pontos de Defesa
*/
int Wizard::getDefensePoints()
{
    double def_pts = Character::getDefensePoints() + ( (double)wisdom/2 );
    return (double)def_pts;
}

/*
 * Nome: attack
 * Descricao: ataca um outro personagem
 * Entrada: (Character*) Referencia a outro personagem
 * Saida: (void)
*/
void Wizard::attack(Character* character)
{
    double miss_chance = 0.1 / XP; // Chance de miss
    
    int damage = 0;

    if( (double)rand()/RAND_MAX <= miss_chance) // Se o personagem errou
    {
        std::cout << "MISS de " << Character::getName() << std::endl;
    }
    else // Se acertou o golpe
    {
        int rnd = (rand()%11) - 5; // Gera um int entre -5 e 5
            
        damage = (getAttackPoints() - character->getDefensePoints()) + rnd; // Calcula o dano
            
        // Se o dano for menor ou igual a zero, utiliza dano 1
        if(damage <= 0)
            damage = 1;

        double critical_chance = 0.02*(XP/2); // Chance de ataque critico

        // Se o personagem conseguiu um ataque critico
        if( (double)rand()/RAND_MAX <= critical_chance )
        {
            damage *= 2;
            std::cout << "CRITICO" << std::endl;
        }

        character->addHP(-damage);
    }

    std::cout << getName() << " causou " << damage << " de dano em " << character->getName() << std::endl;
    std::cout << "    " << character->getName() << " ficou com " << character->getHP() << " de HP." << std::endl;

}

/*
 * Nome: addWisdom
 * Descricao: Adiciona sabedoria
 * Entrada: (int) sabedoria a ser adicionada(ou removido se for negativo)
 * Saida: (void)
*/
void Wizard::addWisdom(int wisdom)
{
    this->wisdom += wisdom;

    if(wisdom < 0) // O valor minimo de power é 0
        wisdom = 0;
}
