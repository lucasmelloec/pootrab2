#include <iostream>

#include "Thief.hpp"

/*
 * Nome: Thief (Construtor)
 * Descricao: Construtor que inicia os atributos
 * Entrada: (std::string) nome do personagem, (int) stealth
*/
Thief::Thief(std::string alias, int stealth) : Character(alias)
{
    this->stealth = stealth;
}

/*
 * Nome: Thief (Construtor)
 * Descricao: Construtor que inicia os atributos a partir de valores dados
 * Entrada: (std::string) nome do personagem, (int) stealth, (int) strength, (int) speed,
 * (int) dexterity, (int) constitution, (int) espaco no inventario
*/
Thief::Thief(std::string alias, int stealth, int strength, int speed, int dexterity, int constitution, int invSpace) :
        Character(alias, strength, speed, dexterity, constitution, invSpace)
{
    this->stealth = stealth;
}

/*
 * Nome: getAttackPoints
 * Descricao: Retorna a quantidade de pontos de ataque
 * Saida: (int) Pontos de Ataque
*/
int Thief::getAttackPoints()
{
    return Character::getAttackPoints() + stealth;
}

/*
 * Nome: getDefensePoints
 * Descricao: Retorna a quantidade de pontos de defesa
 * Saida: (int) Pontos de Defesa
*/
int Thief::getDefensePoints()
{
    return Character::getDefensePoints();
}

/*
 * Nome: attack
 * Descricao: ataca um outro personagem
 * Entrada: (Character*) Referencia a outro personagem
 * Saida: (void)
*/
void Thief::attack(Character* character)
{
    double miss_chance = 0.1 / XP; // Chance de miss
    
    int damage = 0;

    if( (double)rand()/RAND_MAX <= miss_chance) // Se o personagem errou
    {
        std::cout << "MISS de " << Character::getName() << std::endl;
    }
    else // Se acertou o golpe
    {
        int rnd = (rand()%11) - 5; // Gera um int entre -5 e 5
            
        damage = (getAttackPoints() - character->getDefensePoints()) + rnd; // Calcula o dano
            
        // Se o dano for menor ou igual a zero, utiliza dano 1
        if(damage <= 0)
            damage = 1;

        double critical_chance = 0.02*(XP/2); // Chance de ataque critico

        // Se o personagem conseguiu um ataque critico
        if( (double)rand()/RAND_MAX <= critical_chance )
        {
            damage *= 2;
            std::cout << "CRITICO" << std::endl;
        }

        character->addHP(-damage);
    }

    std::cout << getName() << " causou " << damage << " de dano em " << character->getName() << std::endl;
    std::cout << "    " << character->getName() << " ficou com " << character->getHP() << " de HP." << std::endl;

}

/*
 * Nome: addStealth
 * Descricao: Adiciona stealth
 * Entrada: (int) stealth a ser adicionada(ou removido se for negativo)
 * Saida: (void)
*/
void Thief::addStealth(int stealth)
{
    this->stealth += stealth;

    if(stealth < 0) // O valor minimo de power é 0
        stealth = 0;
}
